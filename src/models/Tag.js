const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const TagSchema = new Schema({
  tag: String,
  item: { type: String },
  user: { type: String },
});

module.exports = mongoose.model("tags", TagSchema);
