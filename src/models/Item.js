const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ItemSchema = new Schema({
  title: String,
  username: String,
  password: String,
  url: String,
  commentary: String,
  user: { type: String },
  tag: { type: String },
  title_tag: { type: String },
});

module.exports = mongoose.model("items", ItemSchema);
