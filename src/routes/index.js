// rutas
const express = require("express");
const router = express.Router();

const Item = require("../models/Item");
const User = require("../models/User");
const Tag = require("../models/Tag");

const passport = require("passport");
const { isAuthenticated } = require("../helpers/auth");

///////////////// USER ////////////////////////

router.get("/", (req, res) => {
  res.render("index");
});

router.post("/index/signup", async (req, res) => {
  const newUser = new User(req.body);
  newUser.password = await newUser.encryptPassword(newUser.password);
  await newUser.save();
  res.redirect("/");
});

router.post(
  "/index/signin",
  passport.authenticate("local", {
    successRedirect: "all-items",
    failureRedirect: "/",
    failureFlash: true,
  })
);

router.get("/index/all-items", async (req, res) => {
  const items = await Item.find({ user: req.user.id });
  console.log(items);
  res.render("all-items", {
    items,
  }); //se renderiza el index.ejs
});

router.get("/signout", async (req, res) => {
  req.logout();
  res.redirect("/");
});

/////////////////FIN USER////////////////////////

/////////////////// INICIO ITEM //////////////////

router.get("/new-item", async (req, res) => {
  const tags = await Tag.find({ user: req.user.id });
  console.log(tags);
  res.render("new-item", {
    tags,
  }); //se renderiza el index.ejs
});

router.post("/items/new-item", async (req, res) => {
  const item = new Item(req.body);
  const tags = await Tag.find({ user: req.user.id });
  console.log(item.tag);
  item.user = req.user.id;
  for (var i = 0; i < tags.length; i++) {
    if (tags[i]._id == item.tag) {
      console.log("Parentado");
      tags[i].item = item.title;
      await tags[i].update({ item: item.title });
    } else {
      console.log("No parentado");
    }
  }
  await item.save();
  res.redirect("all-items");
});

router.get("/items/all-items", async (req, res) => {
  const items = await Item.find({ user: req.user.id });
  const tags = await Tag.find({ user: req.user.id });
  console.log(items);
  res.render("all-items", {
    items,
    tags,
  }); //se renderiza el index.ejs
});

router.get("/items", async (req, res) => {
  const items = await Item.find({ user: req.user.id });
  console.log(items);
  res.render("all-items", {
    items,
  }); //se renderiza el index.ejs
});

router.get("/items/delete/:id", async (req, res) => {
  const { id } = req.params;
  const item = await Item.find({ user: req.user.id, _id: id });
  const tags = await Tag.find({ user: req.user.id });
  console.log(item[0].tag);

  for (var i = 0; i < tags.length; i++) {
    if (tags[i]._id == item[0].tag) {
      console.log("Parentado");
      tags[i].item = "";
      await tags[i].update({ item: "" });
    } else {
      console.log("No parentado");
    }
  }

  await Item.remove({
    _id: id,
  });
  res.redirect("/items/all-items");
});

router.get("/items/edit/:id", async (req, res) => {
  const { id } = req.params;
  const item = await Item.findById(id);
  res.render("edit-item", {
    item,
  });
});

router.post("/items/edit/:id", async (req, res) => {
  const { id } = req.params;

  await Item.update(
    {
      _id: id,
    },
    req.body
  );

  res.redirect("/items/all-items");
});

// %%%%%%%%%%%%%%%55 FIN ITEM %%%%%%%%%%%%%%%

// %%%%%%%%%%%%%%%55 INICIO TAG %%%%%%%%%%%%%%%

router.get("/new-tag", (req, res) => {
  res.render("new-tag");
});

router.post("/tags/new-tag", async (req, res) => {
  const tag = new Tag(req.body);
  tag.user = req.user.id;
  await tag.save();
  res.redirect("all-tags");
});

router.get("/all-tags", async (req, res) => {
  const tags = await Tag.find({ user: req.user.id });
  const items = await Item.find();
  console.log(items);
  console.log(tags);
  res.render("all-tags", {
    tags,
    items,
  });
});

router.get("/tags/all-tags", async (req, res) => {
  const tags = await Tag.find({ user: req.user.id });
  console.log(tags);
  res.render("all-tags", {
    tags,
  });
});

router.get("/tags", async (req, res) => {
  const tags = await Tag.find({ user: req.user.id });
  console.log(tags);
  res.render("all-tags", {
    tags,
  });
});

router.get("/tags/delete/:id", async (req, res) => {
  const { id } = req.params;
  await Tag.remove({
    _id: id,
  });
  res.redirect("/tags/all-tags");
});

router.get("/tags/edit/:id", async (req, res) => {
  const { id } = req.params;
  const tag = await Tag.findById(id);
  res.render("edit-tag", {
    tag,
  });
});

router.post("/tags/edit/:id", async (req, res) => {
  const { id } = req.params;
  await Tag.update(
    {
      _id: id,
    },
    req.body
  );
  res.redirect("/tags/all-tags");
});

// %%%%%%%%%%%%%%%55 FIN TAG%%%%%%%%%%%%%%%

module.exports = router;
